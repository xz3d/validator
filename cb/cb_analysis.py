from cb.cb_s3 import *
from pathlib import Path
import pandas as pd
import sqlite3

class cb_analysis:
    def to_sql(filename='target'):
        Path('target.db').touch()
        target = pd.read_csv('target.csv')
        conn = sqlite3.connect('target.db')
        cursor = conn.cursor()
        target.to_sql('target', conn, if_exists='append', index=False)

    def analyze_sql(self, query: str):
        conn = sqlite3.connect('target.db')
        cursor = conn.cursor()
        with open('analysis_output.log', 'w') as f:
            for row in cursor.execute(query):
                f.writelines(str(row))

