from cb.cb_base import *

class cb_cost_explorer(cb_base):
    def __init__(self, result=[]):
        self.result = result
        self.cost_client = super().get_client('ce')
    
    def cost_usage(self, start: str, end: str, gran:str='DAILY', days:int=30, group_type:str='DIMENSION'):
        token = None
        while True:
            if token:
                kwargs = {'NextPageToken': token}
            else:
                kwargs = {}
            data = self.cost_client.get_cost_and_usage(
                TimePeriod={
                    'Start': start, 
                    'End':  end
                }, 
                Granularity=gran, 
                Metrics=['UnblendedCost'], 
                GroupBy=[
                    {'Type': group_type, 'Key': 'LINKED_ACCOUNT'}, 
                    {'Type': group_type, 'Key': 'SERVICE'}
                ], 
                **kwargs
            )
            self.result += data['ResultsByTime']
            token = data.get('NextPageToken')
            if not token:
                break

    def cost_usage_resources(self, start: str, end: str):
        token = None
        while True:
            if token:
                kwargs = {'NextPageToken': token}
            else:
                kwargs = {}
            data = self.cost_client.get_cost_and_usage(
                TimePeriod={
                    'Start': start, 
                    'End':  end
                }, 
                Granularity=gran, 
                Metrics=['UnblendedCost'], 
                GroupBy=[
                    {'Type': 'DIMENSION', 'Key': 'LINKED_ACCOUNT'}, 
                    {'Type': 'DIMENSION', 'Key': 'SERVICE'}
                ], 
                **kwargs
            )
            self.result += data['ResultsByTime']
            token = data.get('NextPageToken')
            if not token:
                break

    def savings_plan_coverage(self, start: str, end: str, gran:str='DAILY', days:int=30, group_type:str='DIMENSION'):
        token = None
        while True:
            if token:
                kwargs = {'NextPageToken': token}
            else:
                kwargs = {}
            data = client.get_savings_plans_coverage(
                TimePeriod={
                    'Start': start,
                    'End': end
                },
                GroupBy=[
                    {
                    'Type': group_type,
                    'Key': 'LINKED_ACCOUNT'
                    },
                ],
                Granularity=gran,
            )

    def cost_categories(self):
        pass

    def cost_forcast(self):
        pass

    def rightsizing_recommendation(self):
        pass
    
    def anomaly_monitors(self):
        pass

    # utility function to publish the cost into tsv file        
    def cost_publish(self):
        # early return
        if self.result == []:
            print("Please call cost before publishing") 
            return
        print('\t'.join(['TimePeriod', 'LinkedAccount', 'Service', 'Amount', 'Unit', 'Estimated']))
        for result_by_time in self.result:
            for group in result_by_time['Groups']:
                amount = group['Metrics']['UnblendedCost']['Amount']
                unit = group['Metrics']['UnblendedCost']['Unit']
                print(result_by_time['TimePeriod']['Start'], '\t', '\t'.join(group['Keys']), '\t', amount, '\t', unit, '\t', result_by_time['Estimated'])
        
    def cost_publish_tsv(self, file):
        if self.result == []:
            print("Please call cost before publishing")
            return
        with open(file, 'w') as f:
            f.write('\t'.join(['TimePeriod', 'LinkedAccount', 'Service', 'Amount', 'Unit', 'Estimated']) + '\n')
            for result_by_time in self.result:
                for group in result_by_time['Groups']:
                    amount = group['Metrics']['UnblendedCost']['Amount']
                    unit = group['Metrics']['UnblendedCost']['Unit']
                    f.write(result_by_time['TimePeriod']['Start'])
                    f.write('\t')
                    f.write('\t'.join(group['Keys']))
                    f.write('\t' + str(amount) + '\t' + str(unit) + '\t' + str(result_by_time['Estimated']) + '\n')
