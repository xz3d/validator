import boto3
import os
import json
import gzip

global key

s3_cl = boto3.client('s3')
def download_dir(prefix, local, bucket, client=s3_cl):
    keys = []
    dirs = []
    next_token = ''
    base_kwargs = {
        'Bucket':bucket,
        'Prefix':prefix,
    }
    while next_token is not None:
        kwargs = base_kwargs.copy()
        if next_token != '':
            kwargs.update({'ContinuationToken': next_token})
        results = client.list_objects_v2(**kwargs)
        contents = results.get('Contents')
        for i in contents:
            k = i.get('Key')
            if k[-1] != '/':
                keys.append(k)
            else:
                dirs.append(k)
        next_token = results.get('NextContinuationToken')
    for d in dirs:
        dest_pathname = os.path.join(local, d)
        if not os.path.exists(os.path.dirname(dest_pathname)):
            os.makedirs(os.path.dirname(dest_pathname))
    for k in keys:
        dest_pathname = os.path.join(local, k)
        if not os.path.exists(os.path.dirname(dest_pathname)):
            os.makedirs(os.path.dirname(dest_pathname))
        client.download_file(bucket, k, dest_pathname)

def download_manifest(date: str):
    download_dir('aquilacloudsbillingreport/aquila/20210' + str(date) + '01-20210' + str(date+1) + '01/aquila-Manifest.json', '.', 'aquilabilling')
    with open('aquilacloudsbillingreport/aquila/20210' + str(date) + '01-20210' + str(date+1) + '01/aquila-Manifest.json', 'r') as f:
        manifest = f.read()
        rr = json.loads(manifest)
        key = rr['reportKeys'][0]
        return str(key)

def download_by_date(date: str, filename='target.csv', fileloc='.'):
    key = download_manifest(date)
    download_dir(key, fileloc, 'aquilabilling')
    with gzip.open(key, 'rt', encoding='utf-8') as ff:
        with open(filename, 'w') as gg:
            for line in ff:
                gg.write(line)