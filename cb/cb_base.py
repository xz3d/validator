import argparse
import boto3
import botocore
import datetime

class cb_base:
    def __init__(self, res="", cl=""):
        self.res = res
        self.cl = cl 

    def get_resource(self, res, loc='ohio'):
        '''
        get_resource is applicable on the following resources:
            - 'cloudformation'
            - 'cloudwatch'
            - 'dynamodb'
            - 'ec2'
            - 'glacier'
            - 'iam'
            - 'opsworks'
            - 's3'
            - 'sns'
            - 'sqs'

            The default value for loc is set to 'ohio'. Can be changed
            pragmatically.
        '''
        return boto3.resource(res, loc)

    def get_client(self, cl, loc='ohio'):
        '''
        A more general abstraction of get_resource();
        Takes modules like cost explorer ('ce')
        '''
        return boto3.client(cl, loc)

    def predl_paginate(self) -> list:
        lst = []
        x = self.get_client('ce')
        
        ce_method_list = [
            'create_anomaly_monitor',
            'create_anomaly_subscription',
            'create_cost_category_definition',
            'delete_anomaly_monitor',
            'delete_anomaly_subscription',
            'delete_cost_category_definition',
            'describe_cost_category_definition',
            'generate_presigned_url',
            'get_anomalies',
            'get_anomaly_monitors',
            'get_anomaly_subscriptions',
            'get_cost_and_usage',
            'get_cost_and_usage_with_resources',
            'get_cost_categories',
            'get_cost_forecast',
            'get_dimension_values',
            'get_paginator',
            'get_reservation_coverage',
            'get_reservation_purchase_recommendation',
            'get_reservation_utilization',
            'get_rightsizing_recommendation',
            'get_savings_plans_coverage',
            'get_savings_plans_purchase_recommendation',
            'get_savings_plans_utilization',
            'get_savings_plans_utilization_details',
            'get_tags',
            'get_usage_forecast',
            'get_waiter',
            'list_cost_category_definitions',
            'provide_anomaly_feedback',
            'update_anomaly_monitor',
            'update_anomaly_subscription',
            'update_cost_category_definition'
        ]

        for i in ce_method_list:
            lst.append(tuple(i, x.can_paginate(i)))

        return lst

def now():
    return datetime.datetime.utcnow()

def start_date(days=30):
    return (now() - datetime.timedelta(days=days)).strftime('%Y-%m-%d')

def end_date():
    t = now()
    return t.strftime('%Y-%m-%d')