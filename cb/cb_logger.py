import logging
import boto3
import botocore

class cb_logging (cb_base):
    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger()
