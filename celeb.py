from cb.cb_cost_explorer import *
from cb.cb_s3 import *
from cb.cb_analysis import *

import pandas as pd
from pathlib import Path

import sqlite3

global queries

ceobj = cb_cost_explorer()

queries = {
    'sankrant.use_case_1' : 
    '''
        select "product/vcpu" as vcpu, "product/version" as version 
        from target where "bill/BillingEntity" == "AWS"
    ''',
   'use_case_2' : 
    '''
       select "lineItem/UsageStartDate" as start_date, "lineItem/UsageEndDate" as end_date 
       from target where "lineItem/ResourceId" == "cloudtrailall"
    ''',
       
    'incurred_lineitem_cost':
     '''
        select "lineItem/UsageStartDate" as start_date, "lineItem/UsageEndDate" as end_date,
        sum(case when "lineItem/LineItemType" = 'DISCOUNTEDUSAGE' then "lineItem/UnBlendedCost"  
        when "lineItem/LineItemType" = 'USAGE' and "lineItem/UnBlendedCost" > 0 and "lineItem/UnBlendedCost" > "lineItem/BlendedCost" then "lineItem/UnBlendedCost" 
        when "lineItem/LineItemType" = 'USAGE' and "lineItem/UnBlendedCost" = 0 then "lineItem/UnBlendedCost" 
        when "lineItem/LineItemType" = 'USAGE' and "lineItem/BlendedCost" > "lineItem/UnBlendedCost" then "lineItem/UnBlendedCost" 
        when "lineItem/LineItemType" = 'SAVINGSPLANCOVEREDUSAGE' and "lineItem/UnBlendedCost" > "lineItem/BlendedCost" then "lineItem/UnBlendedCost"
        else "lineItem/BlendedCost" end) as incurred_cost from target
     '''
}

def publish_cost_usage(filename='ce.tsv'):
    ceobj.cost_usage(start_date(), end_date())
    ceobj.cost_publish_tsv(filename)

def download_aquilabilling_by_month_number(month_num, filename='target.csv'):
    download_by_date(month_num, filename)

def celeb_validation_tool(args):
    if args.command == 'cloud':
        if args.provider == 'aws':
            download_aquilabilling_by_month_number(args.dlmonth, args.fname)
        elif args.cloud == 'azure':
            pass
        elif args.cloud == 'gcp':
            pass
    elif args.command == 'publish':
        publish_cost_usage()
    elif args.command == 'convert':
        if args.sql:
            print("converting to SQL")
            anobj = cb_analysis()
            anobj.to_sql()
    elif args.command == 'analyse':
        if args.query:
            anobj = cb_analysis()
            anobj.analyze_sql(queries[args.query])

def main():
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(dest='command')
    cloud = subparser.add_parser('cloud')
    getce = subparser.add_parser('getce')
    convert = subparser.add_parser('convert')
    analyse = subparser.add_parser('analyse')
    cloud.add_argument('--provider', type=str, default='aws', help='get aws data')
    cloud.add_argument('--dlmonth', type=int, default=5,
                        help='assign month')
    cloud.add_argument('--fname', type=str, default='target.csv', 
                        help='filename')
    getce.add_argument('--publish', action='store_true',
                        help='cost explorer')
    convert.add_argument('--sql', help='convert csv to sql for analysis', action='store_true')
    analyse.add_argument('--query', help='sql query from db', type=str, default='use_case_1')

    args = parser.parse_args()
    celeb_validation_tool(args)

main()